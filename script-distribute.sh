#!/bin/bash
if command -v appcenter > /dev/null; then
    echo "appcenter command exists."
else
	echo "Installing appcenter"
    npm install -g appcenter-cli
fi

echo "Check the android app specs"
appcenter apps show -a dipankardas/demo-1
appcenter build branches list -a dipankardas/demo-1
#appcenter build logs -a dipankardas/demo-1 -i 12
output=$(appcenter build queue -a dipankardas/demo-1 -b ${BRANCH_NAME} --output json)
echo $output | jq .
echo "Triggered distribution to groups"
