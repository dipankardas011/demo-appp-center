#!/bin/bash

# paramaters required are group name, store to distribute, release note

if command -v appcenter > /dev/null; then
    echo "appcenter command exists."
else
	echo "Installing appcenter"
    npm install -g appcenter-cli
fi

get_group=$(appcenter distribute groups show -a dipankardas/demo-1 -g ${GROUP_NAME} --output json | jq '.[1]|.tables[0]')

get_build=$(echo $get_group | jq '.version|tonumber')

appcenter build download -t bundle --id $get_build -a dipankardas/demo-1 -f bundle-latest --debug
unzip bundle-latest.zip
ls -l bundle/app-release.aab
appcenter distribute stores publish -s ${STORE_NAME} -a dipankardas/demo-1 -r "${RELEASE_NOTE}" -f bundle/app-release.aab --debug
