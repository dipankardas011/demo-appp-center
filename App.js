import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Demo Application🤖🔧</Text>
      <Text style={styles.paragraphs}>🚨This is the demo react-native application</Text>
      <Text style={styles.paragraphs}>🚨 Its running on both Android and iOS</Text>
      <Text style={styles.paragraphs}>😇 Tester</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    fontSize: 25,
    fontWeight: "bold",
    color: '#00f',
    padding: 50
  },
  paragraphs: {
    fontSize: 15,
  }
});
